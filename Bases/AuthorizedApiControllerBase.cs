﻿using ElectricMotorbike.Core.Models;
using ElectricMotorbike.Core.Services;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricMotorbike.Core.Bases
{
    [Authorize]
    public abstract class AuthorizedApiControllerBase : ApiControllerBase
    {
        protected readonly ICurrentUserService currentUserService;
        public AuthorizedApiControllerBase(IResponseResultBuilder responseResultBuilder, ICurrentUserService currentUserService) : base(responseResultBuilder)
        {
            this.currentUserService = currentUserService;
        }

    }
}
