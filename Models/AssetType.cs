﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElectricMotorbike.Core.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AssetType
    {
        Avatar,
        Image
    }
}
